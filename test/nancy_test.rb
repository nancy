$:.unshift File.dirname(__FILE__) + '/../sinatra/lib'

require File.dirname(__FILE__) + '/../nancy.rb'
require 'sinatra/test/spec'

context 'Nancy simply' do
  
  setup do
    Page.delete_all
  end
  
  specify "should show the StartPage by default" do
    get_it '/'
    should.be.redirection
    location.should.equal NancyConfig[:start_page]
  end
  
  specify "should ask you to create a page that doesn't exist" do
    get_it '/foo'
    should.be.redirection
    location.should.equal '/foo/edit'
    
    follow!
    
    should.be.ok
    body.should =~ /textarea/
  end
  
  specify "should save a new page" do
    count = Page.count
    post_it '/', 
      :slug => 'foo', 
      :body => '* test', 
      :password => NancyConfig[:password]
      
    Page.count.should.equal count + 1
    
    should.be.redirection
    location.should.equal "/foo"
    
    follow!
    
    body.should =~ /foo/
  end
  
  specify "needs a password to create a page" do
    count = Page.count
    post_it '/', :slug => 'foo', :body => '* test', :password => 'badpass'
    Page.count.should.equal count
    
    status.should.equal 401
  end
  
  specify "is ok with spaces in names" do
    post_it '/', 
      :slug => 'foo bar', 
      :body => '* test', 
      :password => NancyConfig[:password]
    Page.count.should.equal 1
    Page.find(:first).slug.should.equal 'foo bar'
  end
  
  specify "doesn't show banner if available" do
    get_it '/Foo/edit'
    body.scan('<img src="/images/logo.png" id="banner"></img>').size.should.equal 1
    old_banner, NancyConfig[:banner_image] = NancyConfig[:banner_image], nil
    get_it '/Foo/edit'
    body.scan('<img src="/images/logo.png" id="banner"></img>').size.should.equal 0
    NancyConfig[:banner_image] = old_banner
  end
          
end
