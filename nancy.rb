$:.unshift File.dirname(__FILE__) + '/sinatra/lib'
require 'sinatra/mongrel'

require "rubygems"
require "erubis/tiny"
require "data_mapper"
require 'yaml'

begin
  require "superredcloth"
  silence_warnings do
    RedCloth = SuperRedCloth
  end
rescue LoadError
  require 'redcloth'
end

configures do
  DataMapper::Database.setup(
    :adapter  => 'sqlite3',
    :database => File.dirname(__FILE__) + "/nancy.#{Sinatra[:env].to_s}.sqlite3"
  )

  NancyConfig = YAML.load(File.read(File.dirname(__FILE__) + "/nancy.yml"))
end

configures :production do
  get 500 do
    "<h1>We're sooo sorry... something went wrong. Please try agian</h1>"
  end
end

class Page < DataMapper::Base
  property :slug, :string
  property :body, :text
  property :body_html, :text
  property :created_at, :datetime
  property :updated_at, :datetime
end
database.save(Page) unless database.table_exists?(Page)

get 404 do
  "<h1>Unknown page name.  Make sure you don't have /;& in the name</h1>"
end

helpers do
    
  def erbx(key, layout = :layout)
    path = File.dirname(__FILE__) + "/views/%s.erb" % key
    result = Erubis::TinyEruby.new(File.read(path)).evaluate(self)
    if layout && key.to_s !~ /^_/
      erbx(layout, nil) { result }
    else
      result
    end
  end
  
end

get '/' do
  redirect NancyConfig[:start_page]
end

get '/:slug' do
  @page = Page.find(:first, params.pass(:slug))
  if @page
    erbx :show
  else
    redirect "/#{params[:slug].to_param}/edit"
  end
end

get '/:slug/edit' do
  @edit = true
  @page = Page.find(:first, params.pass(:slug)) || Page.new(params.pass(:slug))
  erbx :edit
end

post '/' do
  if params[:password] == NancyConfig[:password]
    @page = Page.find_or_create(params.pass(:slug), {})
    body_html = RedCloth.new(params[:body]).to_html rescue nil
    @page.update_attributes(params.pass(:body).merge(:body_html => body_html))
    redirect "/#{@page.slug.to_param}"
  else
    status 401
    'No way daddy-o'
  end
end
